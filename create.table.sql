create table employees
(
	email varchar(100) not null,
	first_name varchar(200) default '',
	last_name varchar(200) default '',
	updated date default now(),
	deleted boolean default false
);

create unique index employees_email_uindex
	on employees (email);

alter table employees
	add constraint employees_pk
		primary key (email);

alter table employees
	add constraint employees_pk_2
		unique (email);


