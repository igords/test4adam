import log4js from 'log4js';
const logger = log4js.getLogger();

interface IEmployeeFormat {
    email?: string,
    firstName?: string,
    lastName?: string,
    updated?: Date
}

export class Employee implements IEmployeeFormat {
    readonly email: string
    readonly firstName: string
    readonly lastName?: string
    readonly updated: Date

    error: string

    constructor (src: string[]) {
      try {
        this.error = '';
        this.email = src[2] || '';
        this.firstName = src[0] || '';
        this.lastName = src[1] || '';
        this.updated = new Date(src[3]) || new Date();
      } catch (e) {
        logger.warn('Error in ', src);
        this.error = `Invalid row: ${JSON.stringify(src)}`;
      }
    }

    valid (): boolean {
      if (!this.email) this.error += 'Email required';
      if (!this.lastName && !this.firstName) this.error += 'Firstname or lastname required';
      return !this.error;
    }
}
