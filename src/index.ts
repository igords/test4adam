import express from 'express';
import dotenv from 'dotenv';
import log4js from 'log4js';
import { insertData, parseEmployeeFile } from './import';
import * as path from 'path';
dotenv.config();
const logger = log4js.getLogger();

const port = process.env.PORT;
logger.level = process.env.LOG_LEVEL;

const app = express();

/*
logger.info('log4js log info');
logger.debug('log4js log debug');
logger.error('log4js log error');
*/

app.get('/', async (request, response) => {
  try {
    const employees = await parseEmployeeFile(path.join(__dirname, '/../data/demo.csv'));
    const data = await insertData(employees);
    response.json(data);
  } catch (e) {
    response.status(500).json(e.message);
  }
});

app.listen(port, () => console.log(`Running on port ${port}`));
