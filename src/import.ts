import * as csvParse from 'csv-parse';
import parse from 'csv-parse';
import log4js from 'log4js';
import { Employee } from './employee';
import db from './db';

const fs = require('fs');
const logger = log4js.getLogger();

export function parseEmployeeFile (filename: string): Promise<Employee[]> {
  return new Promise((resolve, reject) => {
    let result: Employee[];
    const myParser:csvParse.Parser = parse({ delimiter: ',' }, (err, data) => {
      if (err) {
        reject(err.message);
      }
      result = data.filter((src: string[], i: number) => i > 0 && src.length > 1).map(el => new Employee(el));
      logger.info(result.reduce((a: boolean, el: Employee) => a && el.valid(), true))
      if (!result.reduce((a: boolean, el) => a && el.valid(), true)) { // Validate
        reject(result.filter(res => res.error).map(res => res.error));
      }
      resolve(result);
      logger.info(result.length);
    }) as csvParse.Parser;
    fs.createReadStream(filename).pipe(myParser);
  });
}

const esc = (str: string):string => {
  return `'${str.replace("'", "''")}'`;
};

export function clearDuplicates(employees: Employee[]): Employee[] {
  const data:{ [key:string]: Employee} = {}
  employees.forEach(e => {
    if (!data[e.email] || data[e.email].updated < e.updated) data[e.email] = e;
  })
  return Object.values(data);
}

async function countEmployees(): Promise<number> {
  const qResult = await db.query("SELECT count(email) FROM employees");
  return qResult.rows[0].count;
}

async function markDeleted(employees: Employee[]): Promise<number> {
  const employeeData = clearDuplicates(employees).map(e => esc(e.email)).join(',')
  const query = `UPDATE employees SET deleted=true WHERE email NOT IN (${employeeData})`
  const queryResult =  await db.query(query);
  return queryResult.rowCount;
}

export async function insertData (employees: Employee[]): Promise<{updated:number, created:number, deleted?: number}> {
  const employeeData = clearDuplicates(employees).map((el) => `(${esc(el.email)},
  ${esc(el.firstName)},
  ${esc(el.lastName)}, 
  '${el.updated.toISOString().substring(0, 10)}')`).join(',\n');
  const countBefore = await countEmployees();
  const query = `INSERT INTO employees as em (email, first_name, last_name, updated)
      VALUES ${employeeData}
      ON CONFLICT (email) DO UPDATE
      SET first_name = excluded.first_name,
      last_name = excluded.last_name,
      updated = excluded.updated,
      deleted=false    
      WHERE em.updated < excluded.updated`;
  const queryResult =  await db.query(query);
  const countAfter = await countEmployees();
  const deleted = await markDeleted(employees)
  return {
    updated: queryResult.rowCount,
    created: countAfter - countBefore,
    deleted: deleted
  }
}
