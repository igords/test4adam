import dotenv from 'dotenv';

const { Pool } = require('pg');
dotenv.config();
const db = new Pool({
  database: process.env.PG_DATABASE,
  user: process.env.PG_USER,
  password: process.env.PG_PASSWORD,
  host: process.env.PG_HOST
});

db.connect();

export default db;
